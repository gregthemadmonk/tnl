// Copyright (c) 2004-2023 Tomáš Oberhuber et al.
//
// This file is part of TNL - Template Numerical Library (https://tnl-project.org/)
//
// SPDX-License-Identifier: MIT

#pragma once

//! \brief Namespace for unstructured mesh entity topologies
namespace TNL::Meshes::Topologies {

struct Vertex
{
   static constexpr int dimension = 0;
};

}  // namespace TNL::Meshes::Topologies
