\page UsersGuide Users' Guide

# Table of Content

## Basic topics

1. [General concepts](#ug_GeneralConcepts)
2. [Arrays](#ug_Arrays)
3. [Vectors](#ug_Vectors)
4. [Flexible parallel reduction and scan](#ug_ReductionAndScan)
5. [For loops](#ug_ForLoops)
6. [Sorting](#ug_Sorting)
7. [Cross-device pointers](#ug_Pointers)

## Data structures

1. [Multidimensional arrays](#ug_NDArrays)
2. [Matrices](#ug_Matrices)
3. [Segments (sparse formats)](#ug_Segments)
4. [Orthogonal grids](#ug_Grids)
5. [Unstructured meshes](#ug_Meshes)

## Numerical solvers

1. [Linear solvers](#ug_Linear_solvers)
2. [ODE solvers](#ug_ODE_solvers)
